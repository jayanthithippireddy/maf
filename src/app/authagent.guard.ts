import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import { AuthorizationService } from './customservices/authorization.service';

@Injectable()
export class AuthAgentGuard implements CanActivate {

  constructor(
    private auth: AuthorizationService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      // handle any redirects if a user isn't authenticated
      var authenticatedUser = this.auth.getAuthenticatedUser();
      if (authenticatedUser == null) {
        // redirect the user
        this.router.navigate(['/login']);
        return false;
      }
      else{
      	return true;
      }
  }
}
