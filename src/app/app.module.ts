import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AgentService } from '../agent.service';
import { AuthorizationService } from './customservices/authorization.service';
import { AdminauthorizationService } from './customservices/adminauthorization.service';

import { CookieService } from 'ngx-cookie-service';

import { AuthGuard } from './auth.guard';
import { AuthAgentGuard } from './authagent.guard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AgentService,AdminauthorizationService,CookieService,AuthorizationService,AuthGuard,AuthAgentGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
