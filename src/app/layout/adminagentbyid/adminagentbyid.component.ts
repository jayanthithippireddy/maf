import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import {AuthorizationService} from "../../customservices/authorization.service";


@Component({
  selector: 'app-adminagentbyid',
  templateUrl: './adminagentbyid.component.html',
  styleUrls: ['./adminagentbyid.component.css']
})
export class AdminagentbyidComponent implements OnInit {
showTable:boolean;
  submitted = false;
  rejectPopup = false;
  agentDetail: any;
  rejectform: FormGroup;

  constructor(private agent: AgentService,private agentAuth: AuthorizationService, private router: Router,private cookieService: CookieService,private activatedRoute: ActivatedRoute,private formBuilder: FormBuilder) { }

  ngOnInit() {
    // Reject Form
    this.rejectform = this.formBuilder.group({
      comments: [null, [Validators.required]],
      emailId: [null, [Validators.required]],
      agentId: [null, [Validators.required]],
    });
    const agentId = this.activatedRoute.snapshot.paramMap.get('agentId');
    this.agent.getAgentDetails(agentId)
      .subscribe(res => {
        this.agentDetail = res.data.Item;
      }, (err) => {
        console.log(err);
      });
  }

  onAgentAccepts(agentIds,firstName,emailId,passwords)
  {
  	let data={agentId:agentIds,firstName:firstName,emailId:emailId,password:passwords};

  	let email = data.emailId;
	let password = data.password;
  let agentid = data.agentId;

	this.agentAuth.register(email,password,agentid).subscribe(
	(data) => {
	  	 this.agent.onAgentAccepts(data)
	      .subscribe(res => {
				console.log('done!')
				alert('Verification is Successful. Login Credentials with Send via Email!');
				}, (err) => {
				console.log(err);
				alert('Something Went Wrong.Please try again later');
			});
		},
		(err) => {
		   console.log(err);
	});
  }

  // Reject Method

  onAgentReject() {
    this.rejectPopup = true;
  }
  rfclose() {
    this.rejectPopup = false;
  }
  // Get form
  get f() { return this.rejectform.controls; }
  // Reject Form Submit
  onReject(email,agentid,firstname,user) {
  	console.log('user',email,agentid,firstname,user);
  	let data = {emailId:email,agentid:agentid,firstName:firstname,comment:user.comments};
  	console.log("data",data);
    this.submitted = true;
  	 this.agent.onRejectEmail(data)
      .subscribe(res => {
			console.log('done!')
			alert('Email Had Been Sent To The Agent!');
			}, (err) => {
			console.log(err);
			alert('Something Went Wrong.Please try again later');
		});
  }
}
