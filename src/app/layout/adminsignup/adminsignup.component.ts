import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl,NgForm } from '@angular/forms';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import {AdminauthorizationService} from "../../customservices/adminauthorization.service";

import { Router } from '@angular/router';

@Component({
  selector: 'app-adminsignup',
  templateUrl: './adminsignup.component.html',
  styleUrls: ['./adminsignup.component.css']
})
export class AdminsignupComponent implements OnInit {
  confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  error: string = "";
 submitted = false;
  user: any;
  adminsignupform: FormGroup;
  constructor(private formBuilder: FormBuilder,private adminAuth: AdminauthorizationService,private _router: Router,) { }


  ngOnInit() {
    this.adminsignupform = this.formBuilder.group({
      emailId: [null, [Validators.required]],
      password: [null, Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(15)
      ])],
      confirmpassword: [null, [Validators.required]],
    },{
      validator: MustMatch.MatchPassword
    }
    );
  }

  get ad() { return this.adminsignupform.controls; }
  onSignup(user) {
	    this.user = {
	      emailId: this.adminsignupform.get('emailId').value,
	      password: this.adminsignupform.get('password').value,
	      confirmpassword: this.adminsignupform.get('confirmpassword').value
	    };

	 console.log('cccc',this.user);
    this.submitted = true;
    console.log('Register', this.user);
    if (this.adminsignupform.invalid) {
      console.log('please fill all fields');
      return;
    }

    let email = this.user.emailId;
    let password = this.user.password;

    this.adminAuth.register(email,password).subscribe(
      (data) => {        
        this.confirmCode = false;
        this._router.navigateByUrl('/admin');
      },
      (err) => {
        console.log(err);
        this.error = "Registration Error has occurred";
      }
    );
	}

	
	// validateAuthAdminCode(form: NgForm) {
 //    const code = form.value.code;
 //    this.adminAuth.confirmAuthCode(code).subscribe(
 //      (data) => {
 //        this._router.navigateByUrl('/admin');
 //        this.codeWasConfirmed = true;
 //        this.confirmCode = false;
 //      },
 //      (err) => {
 //        console.log(err);
 //        this.error = "Confirm Authorization Error has occurred";
 //      });
 //  }
}
