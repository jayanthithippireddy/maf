import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import {AuthenticationDetails, CognitoUser, CognitoUserPool,CognitoUserAttribute} from 'amazon-cognito-identity-js';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AdminauthorizationService } from '../../customservices/adminauthorization.service';
import { AuthorizationService } from '../../customservices/authorization.service';



const poolData = {
  UserPoolId: 'us-east-1_mHJWVEl15', // Your user pool id here
  ClientId: '3ahsjgh8mrhf4dvgfsoujaaisl' // Your client id here  
};

const userPool = new CognitoUserPool(poolData);


@Component({
  selector: 'app-agentdashboard',
  templateUrl: './agentdashboard.component.html',
  styleUrls: ['./agentdashboard.component.css']
})
export class AgentdashboardComponent implements OnInit {

  url = '';
  agentDetails :any;
  constructor(private agent: AgentService,private agentAuth: AuthorizationService, private router: Router,private cookieService: CookieService) { }

  ngOnInit() {
     let cognitoUser = userPool.getCurrentUser();

     // console.log('cognitoUser cognitoUser',cognitoUser)

    if (cognitoUser != null) {
       cognitoUser.getSession((err, session) => {
          if (err) {
             return;
          }
          else{
              cognitoUser.getUserAttributes(function(err, result) {
                  if (err) {
                      return;
                  }
                  else{
                    let agentId = result[0].getValue();
                     console.log('agentId agentId',agentId)
                     let data = {agentId:agentId};
                        this.agent.getAgentProfile(data)
                          .subscribe(res => {
                            this.cookieService.set('_Agent_id_', res.data.Item.agentId);
                            this.cookieService.set('_Agent_firstName_', res.data.Item.firstName);
                            this.agentDetails = res.data.Item;
                            console.log('this.agentDetails this.agentDetails',this.agentDetails);
                          }, (err) => {
                            console.log(err);
                          });
                     }
              });
          }
       });
    }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (Event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      };
    }
  }
  // Documents Upload

  uploadFile(event) {
    
    const files: FileList = event.target.files;
    console.log('event', files  );
    if (files.length == 0) {
      console.log('File', files);
      return

    }
  }

 agentLogout(){
  console.log('shyam');  
    this.agentAuth.logOut();
    this.router.navigateByUrl('/login');
  }

}

