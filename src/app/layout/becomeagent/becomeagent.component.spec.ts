import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BecomeagentComponent } from './becomeagent.component';

describe('BecomeagentComponent', () => {
  let component: BecomeagentComponent;
  let fixture: ComponentFixture<BecomeagentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BecomeagentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BecomeagentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
