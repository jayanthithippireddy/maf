import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl,NgForm } from '@angular/forms';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import {AuthorizationService} from "../../customservices/authorization.service";

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  error: string = "";
 submitted = false;
  user: any;
 agentloginform: FormGroup;
  bAuthenticated = false;
  constructor(private formBuilder: FormBuilder,private agentAuth: AuthorizationService,private router: Router) { }
  ngOnInit() {
    this.agentloginform = this.formBuilder.group({
      emailId: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });


    var authenticatedUser = this.agentAuth.getAuthenticatedUser();
    console.log('authenticatedUser',authenticatedUser)
    if (authenticatedUser == null) {
      return;
    }
    this.bAuthenticated = true;
  }

  get ad() { return this.agentloginform.controls; }
  onLogin(user) {
      this.user = {
        emailId: this.agentloginform.get('emailId').value,
        password: this.agentloginform.get('password').value
      };

   console.log('cccc',this.user);
    this.submitted = true;
    console.log('Register', this.user);
    if (this.agentloginform.invalid) {
      console.log('please fill all fields');
      return;
    }

    let email = this.user.emailId;
    let password = this.user.password;

    console.log('cccccccccccccccc',email,password);

    this.agentAuth.signIn(email, password).subscribe(
      (data) => {        
        this.confirmCode = false;
        alert('welcome!')
        this.router.navigateByUrl('/agent-dashboard');
      },
      (err) => {
        console.log(err);
        alert('Email ID or Password Mismatch!')
        this.error = "Registration Error has occurred";
      }
    );
  }
}
