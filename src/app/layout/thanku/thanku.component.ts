import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from "../../customservices/authorization.service";
import {NgForm} from "@angular/forms";
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-thanku',
  templateUrl: './thanku.component.html',
  styleUrls: ['./thanku.component.css']
})
export class ThankuComponent implements OnInit {

  emailVerificationMessage: boolean = false;

  constructor(private auth: AuthorizationService,
              private _router: Router,private cookieService: CookieService) {}


  ngOnInit() {
	}


 onLogin() {
     alert('Please SignIn with the credentials!')
     // let email = this.cookieService.get('_Agent_email_');
     // let password =this.cookieService.get('_Agent_password_');
     //  this.auth.register(email, password).subscribe((data) => {
     //    this._router.navigateByUrl('/login');
     //  }, (err)=> {
     //    this.emailVerificationMessage = true;
     //  }); 

    }  
}

