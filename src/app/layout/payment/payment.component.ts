import { AfterViewInit, Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { of } from 'rxjs';

import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from '../../../projects/ngx-paypal-lib/src/public_api';

import { AgentService } from '../../../agent.service';

import { CookieService } from 'ngx-cookie-service';


// declare var hljs: any;


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

public payPalConfig?: PayPalConfig;

constructor(private router: Router,private agentApi : AgentService,private cookieService: CookieService) { }

  ngOnInit(): void {
    this.initConfig();
  }

  ngAfterViewInit(): void {
    // this.prettify();
  }

  private initConfig(): void {
    this.payPalConfig = new PayPalConfig(
      PayPalIntegrationType.ClientSideREST,
      PayPalEnvironment.Sandbox,
      {
        commit: true,
        client: {
          sandbox:
            'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R'
        },
        button: {
          label: 'paypal',
          layout: 'vertical'
        },
        onAuthorize: (data, actions) => {
          console.log('Authorize');
          return of(undefined);
        },
        onPaymentComplete: (data, actions) => {
          let firstName = this.cookieService.get('_Agent_firstName_');
          let lastName = this.cookieService.get('_Agent_lastName_');
          let emailId = this.cookieService.get('_Agent_email_');
          let phoneNo = this.cookieService.get('_Agent_phone_');
          let agentId = this.cookieService.get('_Agent_id_');
          let sponserId = this.cookieService.get('_Agent_sponserid_');

          let gender = this.cookieService.get('_Agent_gender_');
          let ssnNumber = this.cookieService.get('_Agent_ssnNumber_');
          let residentAddress = this.cookieService.get('_Agent_residentAddress_');
          let residentSuitApartment = this.cookieService.get('_Agent_residentSuitApartment_');
          let residentCity = this.cookieService.get('_Agent_residentCity_');
          let residentState = this.cookieService.get('_Agent_residentState_');
          let residentCountry = this.cookieService.get('_Agent_residentCountry_');
          let residentZipcode = this.cookieService.get('_Agent_residentZipcode_');
          let password = this.cookieService.get('_Agent_password_');


          let final_data = new Object();

              final_data = 
              {
                agentemailId :emailId,
                agentUserName :emailId,
                agentPassword :password,
                phoneNo:phoneNo,
                agentId :agentId,
                sponserId:sponserId,
                firstName:firstName,
                lastName:lastName,
                agentName :firstName,
                amount :"$100",
                paymentToken:data.paymentToken,
                intent:data.intent,
                orderId:data.orderID,
                payerId:data.payerID,
                paymentId:data.paymentID
              }
              console.log('final_data final_data',final_data);

                this.agentApi.postPayment(final_data)
                  .subscribe(res => {
                    alert('Your Payment done Successfully!')

                    this.cookieService.set( '_Agent_paymentToken_',data.paymentToken);
                    this.cookieService.set( '_Agent_orderId_',data.orderID);
                    this.cookieService.set( '_Agent_payerId_',data.payerID);
                    this.cookieService.set( '_Agent_paymentId_',data.paymentID);
                    this.cookieService.set( '_Agent_paymentId_',data.paymentID);

                      this.router.navigate(['/thankyou']);
                    }, (err) => {
                      console.log(err);
                    });
        },
        onCancel: (data, actions) => {
          console.log('OnCancel');
        },
        onError: err => {
          console.log('OnError');
        },
        onClick: () => {
          console.log('onClick');
        },
        validate: (actions) => {
          console.log(actions);
        },
        experience: {
          noShipping: true,
          brandName: 'MAF PAYMEMTS'
        },
        transactions: [
          {
            amount: {
              total: 100.00,
              currency: 'USD',
              details: {
                subtotal: 100.00,
                tax: 0.00,
                shipping: 0.00,
                handling_fee: 0.00,
                shipping_discount: 0.00,
                insurance: 0.00
              }
            },
            custom: 'Custom value',
            item_list: {
              items: [
                {
                  name: 'MAF SUBSCRIPTION',
                  description: 'THE AMOUNT FOR MAF SUBSCRIPTION.',
                  quantity: 1,
                  price: 100,
                  tax: 0.00,
                  sku: '1',
                  currency: 'USD'
                }],
              shipping_address: {
                recipient_name: 'Brian Robinson',
                line1: '4th Floor',
                line2: 'Unit #34',
                city: 'San Jose',
                country_code: 'US',
                postal_code: '95131',
                phone: '011862212345678',
                state: 'CA'
              },
            },
          }
        ],
        payment: () => {
          // create your payment on server side
          return of(undefined);
        },
        note_to_payer: 'Contact us if you have troubles processing payment'
      }
    );
  }

  // private prettify(): void {
  //   hljs.initHighlightingOnLoad();
  // }

}
