import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl,NgForm } from '@angular/forms';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import {AdminauthorizationService} from "../../customservices/adminauthorization.service";

import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
 confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  error: string = "";
 submitted = false;
  user: any;
  adminloginform: FormGroup;
  bAuthenticated = false;
  constructor(private formBuilder: FormBuilder,private adminAuth: AdminauthorizationService,private _router: Router) { }
  ngOnInit() {
    this.adminloginform = this.formBuilder.group({
      emailId: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });


    var authenticatedUser = this.adminAuth.getAuthenticatedUser();
    console.log('authenticatedUser',authenticatedUser)
    if (authenticatedUser == null) {
      return;
    }
    this.bAuthenticated = true;
  }

  get ad() { return this.adminloginform.controls; }
  onLogin(user) {
	    this.user = {
	      emailId: this.adminloginform.get('emailId').value,
	      password: this.adminloginform.get('password').value
	    };

	 console.log('cccc',this.user);
    this.submitted = true;
    console.log('Register', this.user);
    if (this.adminloginform.invalid) {
      console.log('please fill all fields');
      return;
    }

    let email = this.user.emailId;
    let password = this.user.password;

    console.log('cccccccccccccccc',email,password);

    this.adminAuth.signIn(email, password).subscribe(
      (data) => {        
        this.confirmCode = false;
        alert('welcome!')
        this._router.navigateByUrl('/admin-dashboard');
      },
      (err) => {
        console.log(err);
        alert('Email ID or Password Mismatch!')
        this.error = "Registration Error has occurred";
      }
    );
	}
}
