import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AgentService } from '../../../agent.service';
import { MustMatch } from '../../customservices/passwordmatch.validator';

import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerPopup = false;
  passwordPattern: any;
  submitted = false;
  user: any;
  registerform: FormGroup;
  constructor(private formBuilder: FormBuilder, private agent: AgentService,
    private router: Router, private cookieService: CookieService) { }

  ngOnInit() {
    this.passwordPattern = '^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$';
    this.registerform = this.formBuilder.group({
      sponserId: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      gender: [null, [Validators.required]],
      ssnNumber: [null, [Validators.required]],
      // phoneNo: [null, [Validators.required]],
      phoneNo: ['', Validators.compose([
        Validators.required, // Field is required
        Validators.minLength(10),
        Validators.maxLength(10),
      ])],
      emailId: [null, [Validators.required, Validators.email]],
      residentSuitApartment: [null, [Validators.required]],
      residentCity: [null, [Validators.required]],
      residentCountry: [null, [Validators.required]],
      residentState: [null, [Validators.required]],
      residentZipcode: [null, [Validators.required]],
      residentAddress: [null, [Validators.required]],
      isReadyPay: [null, [Validators.required]],
      isAgeVerified: [null, [Validators.required]],
      isAgreeNonRefund: [null, [Validators.required]],
      password: [null, Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(15)
      ])],
      confirmpassword: [null, [Validators.required]],
    }, {
        validator: MustMatch.MatchPassword
      }
    );
  }
  get f() { return this.registerform.controls; }
  onRegister(user) {
    this.user = {
      sponserId: this.registerform.get('sponserId').value,
      firstName: this.registerform.get('firstName').value,
      lastName: this.registerform.get('lastName').value,
      gender: this.registerform.get('gender').value,
      ssnNumber: this.registerform.get('ssnNumber').value,
      phoneNo: this.registerform.get('phoneNo').value,
      emailId: this.registerform.get('emailId').value,
      password: this.registerform.get('password').value,
      confirmpassword: this.registerform.get('confirmpassword').value,
      residentSuitApartment: this.registerform.get('residentSuitApartment').value,
      residentCity: this.registerform.get('residentCity').value,
      residentCountry: this.registerform.get('residentCountry').value,
      residentState: this.registerform.get('residentState').value,
      residentZipcode: this.registerform.get('residentZipcode').value,
      residentAddress: this.registerform.get('residentAddress').value,
      isReadyPay: this.registerform.get('isReadyPay').value,
      isAgeVerified: this.registerform.get('isAgeVerified').value,
      isAgreeNonRefund: this.registerform.get('isAgreeNonRefund').value,

    };

    console.log('cccc', this.user);
    this.submitted = true;
    console.log('Register', this.user);
    if (this.registerform.invalid) {
      console.log('please fill all fields');
      return;
    }
    this.agent.registerPost(this.user)
      .subscribe(res => {
        this.cookieService.set('_Agent_sponserid_', res.data.MainData.sponserId);
        this.cookieService.set('_Agent_id_', res.data.MainData.agentId);
        this.cookieService.set('_Agent_firstName_', res.data.MainData.firstName);
        this.cookieService.set('_Agent_lastName_', res.data.MainData.lastName);
        this.cookieService.set('_Agent_email_', res.data.MainData.emailId);
        this.cookieService.set('_Agent_phone_', res.data.MainData.phoneNo);
        this.cookieService.set('_Agent_gender_', res.data.MainData.gender);
        this.cookieService.set('_Agent_ssnNumber_', res.data.MainData.ssnNumber);
        this.cookieService.set('_Agent_residentAddress_', res.data.MainData.residentAddress);
        this.cookieService.set('_Agent_residentSuitApartment_', res.data.MainData.residentSuitApartment);
        this.cookieService.set('_Agent_residentCity_', res.data.MainData.residentCity);
        this.cookieService.set('_Agent_residentState_', res.data.MainData.residentState);
        this.cookieService.set('_Agent_residentCountry_', res.data.MainData.residentCountry);
        this.cookieService.set('_Agent_residentZipcode_', res.data.MainData.residentZipcode);
        this.cookieService.set('_Agent_password_', res.data.MainData.password);
        this.registerPopup = true;
        // this.router.navigate(['/payment']);
      }, (err) => {
        console.log(err);
      });
  }
  // popup() {
  //   this.registerPopup = true;
  // }


}
