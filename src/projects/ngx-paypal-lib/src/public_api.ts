import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

let httpHeaders = new HttpHeaders({
     'content-Type' : 'application/json',
   
}); 

const httpOptions = {
  headers: httpHeaders
};

/** Public API */
export * from './lib/ngx-paypal.module';
export * from './lib/models';
export * from './lib/components';

